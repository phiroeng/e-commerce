import { defineStore } from "pinia";
import { ref } from "vue";
export const useStore = defineStore("store", () => {
  // GLOBLE VARIABLE
  const categoryId = ref("");

  // FUNCTIONS
  const handleSetValueCategory = (id) => {
    categoryId.value = id;
  };

  // RETURN VALUE
  return {
    handleSetValueCategory,
    categoryId,
  };
});
