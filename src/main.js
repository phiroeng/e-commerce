import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";
import router from "./router";
import { createPinia } from "pinia";
import { browserLocalPersistence, setPersistence } from "firebase/auth";
import { projectAuth } from "./firebase/config";
import { createI18n } from "vue-i18n";

// SW LANG
import en from "./i18n/en.json";
import kh from "./i18n/kh.json";

const i18n = createI18n({
  legacy: false,
  locale: "en",
  fallbackLocale: "en",
  messages: {
    en,
    kh,
  },
});
const pinia = createPinia();
const app = createApp(App);
setPersistence(projectAuth, browserLocalPersistence).then(() => {
  app.use(pinia);
  app.use(router);
  app.use(i18n);
  app.mount("#app");
});
