import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyD9hO6JIheVQweZWO2H9vIOwkocDIPZq4M",
  authDomain: "ecommerce-pro-45c05.firebaseapp.com",
  projectId: "ecommerce-pro-45c05",
  storageBucket: "ecommerce-pro-45c05.appspot.com",
  messagingSenderId: "803534553583",
  appId: "1:803534553583:web:cb83972ef1f8044f1d361a",
};

//firebase inz
const app = initializeApp(firebaseConfig);

const projectFireStore = getFirestore(app); // useFirestore
const projectAuth = getAuth(app); // useAuth
const projectStorage = getStorage(app); // useStorage
//export for use
export { projectFireStore, projectAuth, projectStorage };
