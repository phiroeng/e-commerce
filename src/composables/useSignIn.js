import { projectAuth } from "../firebase/config";
import { ref } from "vue";
import { signInWithEmailAndPassword } from "firebase/auth";

const error = ref(null);
const isPending = ref(true);

const handleSignIn = async (formData) => {
  try {
    error.value = null;
    isPending.value = true;
    await signInWithEmailAndPassword(
      projectAuth,
      formData.email,
      formData.password
    );
    return true;
  } catch (err) {
    error.value = err.message;
  } finally {
    isPending.value = false;
  }
};

export { handleSignIn, error, isPending };
