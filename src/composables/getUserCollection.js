import { ref } from "vue";
import { projectAuth } from "../firebase/config";
import { onAuthStateChanged, signOut } from "firebase/auth";

const getUser = ref(null);

onAuthStateChanged(projectAuth, (user) => {
  if (user) {
    // console.log("userExist", user);
    getUser.value = user;
  } else {
    getUser.value = null;
  }
});

const handleSignOut = async () => {
  try {
    await signOut(projectAuth);
  } catch (error) {
    console.log("handleSignOutError", error);
  }
};

export { getUser, handleSignOut };
