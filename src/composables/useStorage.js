import { projectStorage } from "../firebase/config";
import { ref } from "vue";
import { getUser } from "./getUserCollection";
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
  deleteObject,
} from "firebase/storage";

const getUrl = ref("");

// uplaod image
const handleUploadImage = async (file) => {
  const filePath = `images/${getUser.value.uid}/${file.name}`;
  const fileStorageRef = storageRef(projectStorage, filePath);
  try {
    await uploadBytes(fileStorageRef, file);
    getUrl.value = await getDownloadURL(fileStorageRef);
    console.log("Upload successful: ", getUrl.value);
    return true;
  } catch (err) {
    console.log("Error uploading file: ", err);
  }
};

const handleDeleteImage = async (imageUrl) => {
  const fileStorageRef = storageRef(projectStorage, imageUrl);
  try {
    await deleteObject(fileStorageRef);
    return true;
  } catch (error) {
    console.log("message", error);
  }
};

export { handleUploadImage, handleDeleteImage, getUrl };
