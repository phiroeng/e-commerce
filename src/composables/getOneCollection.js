import { ref } from "vue";
import { projectFireStore } from "../firebase/config";
import { getDoc, doc } from "firebase/firestore";

const getOneCollection = (collectionName) => {
  const documents = ref(null);

  // FindOne
  const handleFindOneDoc = async (id) => {
    const collectionRef = doc(projectFireStore, collectionName, id);
    const item = await getDoc(collectionRef);
    if (item.exists()) {
      documents.value = { id: id, ...item.data() };
      console.log(documents.value);
    } else {
      console.log("No such document!");
    }
  };

  return { handleFindOneDoc, documents };
};

export default getOneCollection;
