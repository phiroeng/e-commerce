import { ref, watchEffect } from "vue";
import { projectFireStore } from "../firebase/config";
import { collection, onSnapshot, query } from "firebase/firestore";

const getUserDoc = (collectionName) => {
  const isLoading_user = ref(true);
  const userDoc = ref(null);
  const collectionRef = query(collection(projectFireStore, collectionName));
  const unsubscripe = onSnapshot(collectionRef, (qry) => {
    const result = [];
    qry.forEach((doc) => {
      result.push({ id: doc.id, ...doc.data() });
    });
    userDoc.value = result;
    isLoading_user.value = false;
  });

  watchEffect((onInvalidate) => {
    onInvalidate(() => unsubscripe());
  });

  return { userDoc, isLoading_user };
};

export default getUserDoc;
