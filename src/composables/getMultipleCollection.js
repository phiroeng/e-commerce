import { projectFireStore } from "../firebase/config";
import { collection, query, getDocs, where } from "firebase/firestore";
import { ref } from "vue";

const getMultipleCollection = (collectionName) => {
  const documents = ref([]);
  const isLoading = ref(false);

  //Find Multiple
  const handleFindMultipleDoc = async (queryField, id) => {
    isLoading.value = true;
    const collectionRef = await query(
      collection(projectFireStore, collectionName),
      where(queryField, "==", id)
    );

    const listDoc = await getDocs(collectionRef);
    if (listDoc) {
      const result = [];
      listDoc.forEach((doc) => {
        result.push({ id: doc.id, ...doc.data() });
      });
      documents.value = result;
      isLoading.value = false;
    }
  };
  return { handleFindMultipleDoc, documents };
};

export default getMultipleCollection;
