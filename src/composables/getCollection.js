import { ref, watchEffect } from "vue";
import { projectFireStore } from "../firebase/config";
import {
  collection,
  onSnapshot,
  orderBy,
  query,
  Timestamp,
  where,
} from "firebase/firestore";

const getCollection = (collectionName, date) => {
  const isLoading = ref(true);
  const documents = ref([]);
  const collectionRef = ref(null);

  if (date && date.start_date && date.end_date) {
    collectionRef.value = query(
      collection(projectFireStore, collectionName),
      orderBy("created", "desc"),
      where("created", ">=", date.start_date),
      where("created", "<=", date.end_date)
    );
  } else {
    collectionRef.value = query(collection(projectFireStore, collectionName));
  }

  const unsubscripe = onSnapshot(collectionRef.value, (qry) => {
    const result = [];
    qry.forEach((doc) => {
      result.push({ id: doc.id, ...doc.data() });
    });
    documents.value = result;
    isLoading.value = false;
  });

  watchEffect((onInvalidate) => {
    onInvalidate(() => unsubscripe());
  });

  return { documents, isLoading };
};

export default getCollection;
