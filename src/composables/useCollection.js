import { projectFireStore } from "../firebase/config";
import {
  collection,
  addDoc,
  deleteDoc,
  doc,
  updateDoc,
  setDoc,
} from "firebase/firestore";
import { ref } from "vue";
const loading = ref(false);
const error = ref("");

const useCollection = (collectionName) => {
  let collectionRef = collection(projectFireStore, collectionName); //items

  const handleAddDoc = async (formDoc) => {
    loading.value = true;
    try {
      await addDoc(collectionRef, formDoc);
      return true;
    } catch (err) {
      console.log("insert doc fail", err);
      error.value = err.message;
      loading.value = false;
    }
  };

  const handleSetDoc = async (id, formDoc) => {
    await setDoc(doc(collectionRef, id), formDoc);
    return true;
  };

  const handleRemoveDoc = async (id) => {
    try {
      await deleteDoc(doc(collectionRef, id));
      return true;
    } catch (err) {
      console.log("Failed to delete data", err);
      return false;
    }
  };

  const handleUpdateDoc = async (id, formDoc) => {
    try {
      console.log("Updating document with ID:", id);
      console.log("Updated data:", formDoc);

      await updateDoc(doc(collectionRef, id), formDoc);
      console.log("Document updated successfully");

      return true; // Return true to indicate success
    } catch (error) {
      console.error("Error updating document:", error);
      return false; // Return false to indicate failure
    }
  };

  const handleAddUser = async (userData) => {
    try {
      const result = await addDocs(userData);
      console.log("User added successfully with ID:", result.id);
      return result.id;
    } catch (error) {
      console.error("Error adding user:", error);
      return false;
    }
  };

  return {
    handleAddDoc,
    handleSetDoc,
    handleRemoveDoc,
    handleUpdateDoc,
    handleAddUser,
    loading,
    error,
  };
};

export default useCollection;
