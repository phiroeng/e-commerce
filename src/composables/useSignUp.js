import { projectAuth } from "../firebase/config";
import { ref } from "vue";
import { createUserWithEmailAndPassword, updateProfile } from "firebase/auth";

const error = ref(null);
const isPending = ref(null);

const handleSignUp = async (formData) => {
  try {
    error.value = null;
    isPending.value = true;
    const res = await createUserWithEmailAndPassword(
      projectAuth,
      formData.email,
      formData.password,
      formData.name
    );
    await updateProfile(res.user, { displayName: formData.name });
    return res;
  } catch (err) {
    error.value = err.message;
  } finally {
    isPending.value = false;
  }
};

export { handleSignUp, error, isPending };
