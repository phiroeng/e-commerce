export const formatCurrency = (value, currency, lang = undefined) => {
  return Intl.NumberFormat(lang, {
    style: "currency",
    currency: currency,
  }).format(value);
};
