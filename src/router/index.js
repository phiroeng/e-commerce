import { createRouter, createWebHistory } from "vue-router";
const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("../layouts/MainLayout.vue"),
    children: [
      {
        path: "/",
        name: "Dashboard",
        component: () => import("../layouts/Dashboard.vue"),
      },
      {
        path: "/home",
        name: "Home",
        component: () => import("../pages/Home.vue"),
      },
      {
        path: "/post",
        name: "Post",
        component: () => import("../pages/PostCreate.vue"),
      },
      {
        path: "/post/:id",
        name: "postId",
        component: () => import("../pages/PostCreate.vue"),
      },
      {
        path: "/category",
        name: "Category",
        component: () => import("../pages/Category.vue"),
      },
      {
        path: "/category/:id",
        name: "CategoryDetail",
        component: () => import("../pages/CategoryDetails.vue"),
      },
      {
        path: "/signup",
        name: "Signup",
        component: () => import("../layouts/SignUp.vue"),
      },
      {
        path: "/signin",
        name: "Signin",
        component: () => import("../layouts/SignIn.vue"),
      },
      {
        path: "/card_details",
        name: "CardDetails",
        component: () => import("../pages/CardDetails.vue"),
      },
      {
        path: "/order/checkout_order",
        name: "CheckoutOrder",
        component: () => import("../pages/CheckoutOrder.vue"),
      },
      {
        path: "/reports/list_report",
        name: "ListReport",
        component: () => import("../Reports/ListReport.vue"),
      },
      {
        path: "/404",
        name: "NotFound",
        component: () => import("../layouts/PageNotFound.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
